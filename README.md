# Rich Text PoC

Playing around with inline rich text editing.

## Usage

Install dependencies: `npm install`

Run dev: `npm run dev`

Build static resources: `npm run build`