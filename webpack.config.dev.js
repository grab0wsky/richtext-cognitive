var webpackConfig = require('./webpack.config');

webpackConfig.devtool = 'eval';

webpackConfig.output = {
    pathinfo: true,
    publicPath: 'http://0.0.0.0:8080/',
    filename: '[name].js'
};

module.exports = webpackConfig;
