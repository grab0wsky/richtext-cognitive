import initializeToolbar from "./toolbar";
import initializeTooltip from "./tooltip";
import initializeDebug, {registerEventListeners as debugListen} from "./debug";
import {registerEventListeners as paragraphListen, registerGlobalEventListeners} from "./paragraphs";

// Initialize component
window.addEventListener('load', () => {

  // Add tooltip to DOM
  initializeTooltip();

  // Add toolbar to DOM
  initializeToolbar();

  // Debugging
  initializeDebug();

  // Initialize global event listeners
  registerGlobalEventListeners();

  // Initialize editors, 1st find all editors
  let editors = document.getElementsByClassName('richtext-editor');

  Array.prototype.filter.call(editors, (editor) => {

    // Set height to container height
    editor.style.minHeight = editor.parentElement.offsetHeight + 'px';

    // Add event listeners to each paragraph
    addEventListeners(editor);

    // Set editable
    editor.setAttribute('contenteditable', 'true');
  });

});

let addEventListeners = (editor) => {
  // Prevent certain key events
  editor.addEventListener('keypress', denyEventListener, false);
  editor.addEventListener('keydown', denyEventListener, false);
  editor.addEventListener('keyup', denyEventListener, false);

  // Paste event
  editor.addEventListener('paste', pasteEventListener, false);

  // Register other event listeners
  debugListen(editor);
  paragraphListen(editor);
};

let pasteEventListener = (event) => {
  event.preventDefault();

  // Get pasted data via clipboard API
  let clipboardData = event.clipboardData || window.clipboardData;
  let pastedData = clipboardData.getData('text/html');

  console.log('Pasting is disabled for now, your HTML data would have been: %o', pastedData);
};

let denyEventListener = (event) => {
  if (denyKeyInput(event)) {
    event.preventDefault();
  } else {
    // Do nothing
  }
};

// Verify input
// Deny
let denyKeyInput = (event) =>
         event.keyCode === 13 && event.shiftKey // Shit + Enter
      || event.keyCode === 9                    // Tab
    //|| event.keyCode === 13 && getCurrentParagraph().nodeName !== TYPE_P // Enter in anything other than a P
;
