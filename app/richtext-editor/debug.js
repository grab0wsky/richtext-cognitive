// --
// DEBUG
let rawCode = null;

let initializeDebug = () => {
  // Initialize debug container
  rawCode = document.getElementById('raw-code');
};

// If node is null, return null
// If node is a DIV and the DIV is contenteditable, then we found the editor DOM element,
// if not, recursively walk up
let findEditorNode = (node) => node === null
|| (node.nodeName === 'DIV' && node.getAttribute('contenteditable') === 'true')
    ? node
    : findEditorNode(node.parentNode);

let debug = (target) => {
  let editor = findEditorNode(target);
  if (rawCode !== null && editor !== null) {
    rawCode.innerHTML = escapeHtml(editor.innerHTML);
  }
};

let escapeHtml = (unsafe) =>
  unsafe
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;')
    .replace(/&lt;/g, '<br />&lt;')
    .replace(/&gt;/g, '&gt;<br />');

let registerEventListeners = (editor) => {
  // create an observer instance
  let observer = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
      debug(mutation.target);
    });
  });

  // configuration of the observer:
  let config = { attributes: true, childList: true, characterData: true, subtree: true };

  // pass in the target node, as well as the observer options
  observer.observe(editor, config);
};

export default initializeDebug;
export { registerEventListeners }
