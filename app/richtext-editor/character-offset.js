let getCaretCharacterOffsetWithin = (node) => {
  let caretOffset = 0;
  let doc = node.ownerDocument || node.document;
  let win = doc.defaultView || doc.parentWindow;
  let sel;
  if (typeof win.getSelection !== 'undefined') {
    sel = win.getSelection();
    if (sel.rangeCount > 0) {
      let range = win.getSelection().getRangeAt(0);
      let preCaretRange = range.cloneRange();
      preCaretRange.selectNodeContents(node);
      preCaretRange.setEnd(range.endContainer, range.endOffset);
      caretOffset = preCaretRange.toString().length;
    }
  } else if ( (sel = doc.selection) && sel.type !== 'Control') {
    let textRange = sel.createRange();
    let preCaretTextRange = doc.body.createTextRange();
    preCaretTextRange.moveToElementText(node);
    preCaretTextRange.setEndPoint("EndToEnd", textRange);
    caretOffset = preCaretTextRange.text.length;
  }
  return caretOffset;
};

let setCaretCharacterOffsetWithin = (node, position) => {
  node.focus();
  let textNode = node.firstChild;
  let range = document.createRange();
  range.setStart(textNode, position);
  range.setEnd(textNode, position);
  let sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
};

let getCaretParentNodeWithin = (node) => {
  let parentNode = null;

  let doc = node.ownerDocument || node.document;
  let win = doc.defaultView || doc.parentWindow;
  let sel;
  if (typeof win.getSelection !== 'undefined') {
    sel = win.getSelection();
    if (sel.rangeCount > 0) {
      let range = win.getSelection().getRangeAt(0);
      parentNode = range.startContainer.nodeName === '#text' ? range.startContainer.parentNode : range.startContainer;
    }
  } else if ( (sel = doc.selection) && sel.type !== 'Control') {
    let textRange = sel.createRange();
    let range = doc.body.createTextRange();
    parentNode = range.startContainer.nodeName === '#text' ? range.startContainer.parentNode : range.startContainer;
  }

  return parentNode;
};

export { getCaretCharacterOffsetWithin, setCaretCharacterOffsetWithin, getCaretParentNodeWithin }