import { getCaretCharacterOffsetWithin, setCaretCharacterOffsetWithin, getCaretParentNodeWithin } from './character-offset';

const TYPE_P = 'P';
const TYPE_H1 = 'H1';
const TYPE_H2 = 'H2';
const TYPE_H3 = 'H3';
const TYPE_OL = 'OL';
const TYPE_UL = 'UL';
const TYPE_BLOCKQUOTE = 'BLOCKQUOTE';

const ALLOWED_TYPES = [ TYPE_P, TYPE_H1, TYPE_H2, TYPE_H3, TYPE_OL, TYPE_UL, TYPE_BLOCKQUOTE, 'BR', 'SPAN' ];

let currentActiveParagraph = null, paragraphChangeSubscribers = [], currentActiveNode = null;

let toggleParagrahTypeTo = (nodeName, node = currentActiveParagraph) => {
  // Revert to P, if target nodeName is already set
  if (node.nodeName === nodeName) {
    nodeName = 'P';
  }

  // Create new node and copy content
  let newNode = document.createElement(nodeName);
  newNode.innerHTML = node.innerHTML;

  // Get character offset in current node
  let offset = getCaretCharacterOffsetWithin(node);

  // TODO Remove debug message
  console.log('Changing %o to %o', currentActiveParagraph, newNode);

  // Swap nodes
  currentActiveParagraph.parentNode.replaceChild(newNode, node);
  currentActiveParagraph = newNode;
  // TODO Correctly set caret in sub nodes
  currentActiveNode = newNode;

  // Set character offset in new node
  setCaretCharacterOffsetWithin(newNode, offset);

  notifySubscribers(newNode, newNode);
};

let activeParagraphEventListener = (event) => {
  let node = document.getSelection().anchorNode;

  // Skip processing, if we're in a slate
  if (isInSlate(node)) {
    event.stopPropagation();
    event.preventDefault();
    return;
  }

  // Find corresponding Editor node
  node = findEditorNode(node);

  // Find parent element of cursor start position
  if (node !== null) {
    node = getCaretParentNodeWithin(node);
  }

  // Find the nearest allowed root node
  if (node !== null) {
    currentActiveNode = node;
    node = findNearestAllowedRoot(node);
  }

  // If node is different than currently active node, update active node and notify subscribers of change
  //if (node !== currentActiveParagraph) {
    currentActiveParagraph = node;
    notifySubscribers();
  //}
};

let findNearestAllowedRoot = (node) => ALLOWED_TYPES.includes(node.nodeName) ? node : findNearestAllowedRoot(node.parentNode);

let findEditorNode = (node) => {
  // If node is null, return null
  if (node === null) {
    return null;
  }

  // If node is a DIV and the DIV is contenteditable, then we found the editor DOM element,
  // if not, recursively walk up
  if (node.nodeName === 'DIV' && node.getAttribute('contenteditable') === 'true') {
    // Check if editor has paragraphs ...
    if (node.children.length === 0) {
      // .. if not, add one and return it
      let newParagraph = createNewParagraph();
      node.appendChild(newParagraph);
      setCaretCharacterOffsetWithin(newParagraph, 0);
    }
    return node;
  } else {
    return findEditorNode(node.parentNode);
  }
};

let createNewParagraph = () => {
  let paragraph = document.createElement('P');
  paragraph.appendChild(document.createElement('BR'));
  return paragraph;
};


let notifySubscribers = () =>
  paragraphChangeSubscribers.forEach(callback => callback(currentActiveParagraph, currentActiveNode));

let subscribeToParagraphChange = (callback) => paragraphChangeSubscribers.push(callback);

let getCurrentParagraph = () => currentActiveParagraph;
let getCurrentNode = () => currentActiveNode;

let registerGlobalEventListeners = () => {
  // Listen on active paragraph change
  document.addEventListener('keyup', activeParagraphEventListener, false);
  document.addEventListener('mouseup', activeParagraphEventListener, false);
};

let registerEventListeners = (editor) => {
  // Initialize observer
  initializeObserver(editor);
};

let initializeObserver = (editor) => {
  // create an observer instance
  let observer = new MutationObserver(validateMutations);

  // configuration of the observer:
  let config = {childList: true};

  // pass in the target node, as well as the observer options
  observer.observe(editor, config);
};

// Ensure that only allowed top level types are added to the editor
let validateMutations = (mutations) => {
  mutations.forEach((mutation) => {
    mutation.addedNodes.forEach((addedNode) => {
      if (!(addedNode.nodeName === 'DIV' && addedNode.classList.contains('slate')) && !ALLOWED_TYPES.includes(addedNode.nodeName)) {
        // TODO This could be a bit smarter …
        toggleParagrahTypeTo(TYPE_P, addedNode);
      }
    })
  })
};

let unselectParagraph = () => {
  currentActiveParagraph = null;
};

export {
  toggleParagrahTypeTo, subscribeToParagraphChange, getCurrentParagraph,
  createNewParagraph, registerEventListeners, unselectParagraph, registerGlobalEventListeners,
  TYPE_P, TYPE_H1, TYPE_H2, TYPE_H3, TYPE_OL, TYPE_UL, TYPE_BLOCKQUOTE
}