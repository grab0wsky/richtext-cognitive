import {
  subscribeToParagraphChange,
  toggleParagrahTypeTo,
  TYPE_BLOCKQUOTE,
  TYPE_H1,
  TYPE_H2,
  TYPE_OL,
  TYPE_UL
} from "./paragraphs";

const BUTTON_DISABLED = 0;
const BUTTON_ENABLED = 1;
const BUTTON_ACTIVE = 2;

const buttonStateConfig = {
  P: {
    'rt-f-h1': BUTTON_ENABLED,
    'rt-f-h2': BUTTON_ENABLED,
    'rt-f-b': BUTTON_ENABLED,
    'rt-f-i': BUTTON_ENABLED,
    'rt-f-quote': BUTTON_ENABLED,
    'rt-f-bulleted': BUTTON_ENABLED,
    'rt-f-numbered': BUTTON_DISABLED
  },
  B: {
    'rt-f-b': BUTTON_ACTIVE
  },
  I: {
    'rt-f-i': BUTTON_ACTIVE
  },
  H1: {
    'rt-f-h1': BUTTON_ACTIVE,
    'rt-f-h2': BUTTON_ENABLED,
    'rt-f-b': BUTTON_ENABLED,
    'rt-f-i': BUTTON_ENABLED,
    'rt-f-quote': BUTTON_ENABLED,
    'rt-f-bulleted': BUTTON_DISABLED,
    'rt-f-numbered': BUTTON_DISABLED
  },
  H2: {
    'rt-f-h1': BUTTON_ENABLED,
    'rt-f-h2': BUTTON_ACTIVE,
    'rt-f-b': BUTTON_ENABLED,
    'rt-f-i': BUTTON_ENABLED,
    'rt-f-quote': BUTTON_ENABLED,
    'rt-f-bulleted': BUTTON_DISABLED,
    'rt-f-numbered': BUTTON_DISABLED
  },
  UL: {
    'rt-f-h1': BUTTON_DISABLED,
    'rt-f-h2': BUTTON_DISABLED,
    'rt-f-b': BUTTON_ENABLED,
    'rt-f-i': BUTTON_ENABLED,
    'rt-f-quote': BUTTON_DISABLED,
    'rt-f-bulleted': BUTTON_ACTIVE,
    'rt-f-numbered': BUTTON_DISABLED
  },
  BLOCKQUOTE: {
    'rt-f-h1': BUTTON_ENABLED,
    'rt-f-h2': BUTTON_ENABLED,
    'rt-f-b': BUTTON_ENABLED,
    'rt-f-i': BUTTON_ENABLED,
    'rt-f-quote': BUTTON_ACTIVE,
    'rt-f-bulleted': BUTTON_DISABLED,
    'rt-f-numbered': BUTTON_DISABLED
  },
  disabled: {
    'rt-f-h1': BUTTON_DISABLED,
    'rt-f-h2': BUTTON_DISABLED,
    'rt-f-b': BUTTON_DISABLED,
    'rt-f-i': BUTTON_DISABLED,
    'rt-f-quote': BUTTON_DISABLED,
    'rt-f-bulleted': BUTTON_DISABLED,
    'rt-f-numbered': BUTTON_DISABLED
  }
};

toolbar = null;

let initializeToolbar = () => {
  // Create toolbar
  toolbar = document.createElement('DIV');
  toolbar.className = 'richtext-editor-toolbar card card-2';
  toolbar.innerHTML =
      '<button role="button" disabled="disabled" class="rt-f-h1"><span>H1</span></button>' +
      '<button role="button" disabled="disabled" class="rt-f-h2"><span>H2</span></button>' +
      '<button role="button" disabled="disabled" class="rt-f-b"><i class="material-icons">format_bold</i></button>' +
      '<button role="button" disabled="disabled" class="rt-f-i"><i class="material-icons">format_italic</i></button>' +
      '<button role="button" disabled="disabled" class="rt-f-quote"><i class="material-icons">format_quote</i></button>' +
      '<button role="button" disabled="disabled" class="rt-f-bulleted"><i class="material-icons">format_list_bulleted</i></button>' +
      '<button role="button" disabled="disabled" class="rt-f-numbered"><i class="material-icons">format_list_numbered</i></button>';

  Array.prototype.filter.call(
      toolbar.getElementsByTagName('button'),
      (button) => button.addEventListener('mousedown', (event) => toolbarClickEventListener(event, button), false)
  );

  // Append toolbar to body
  Array.prototype.filter.call(document.getElementsByTagName('body'), (body) => body.appendChild(toolbar));

  // Subscribe to paragraph changes
  subscribeToParagraphChange(updateToolbar);
};

let toolbarClickEventListener = (event, button) => {
  let type = button.className;
  switch (type) {
    case 'rt-f-h1':
      toggleParagrahTypeTo(TYPE_H1);
      break;

    case 'rt-f-h2':
      toggleParagrahTypeTo(TYPE_H2);
      break;

    case 'rt-f-bulleted':
      console.log('Lists are not supprted yet.');
      document.execCommand('insertUnorderedList');
      break;

    case 'rt-f-numbered':
      console.log('Lists are not supprted yet.');
      document.execCommand('insertOrderedList');
      break;

    case 'rt-f-quote':
      toggleParagrahTypeTo(TYPE_BLOCKQUOTE);
      break;

    case 'rt-f-b':
      document.execCommand('bold');
      break;

    case 'rt-f-i':
      document.execCommand('italic');
      break;
  }
  event.preventDefault();
};

let updateToolbar = (activeParagraph, activeNode) => {
  let config = null;

  // First, set paragraph configuration
  if (!activeParagraph) {
    config = buttonStateConfig.disabled;
  }
  else {
    config = buttonStateConfig[activeParagraph.nodeName];
  }

  // Then overlay active node config
  // TODO Need to traverse up to find all nested nodes, like <i><b>text</b></i>
  if (activeNode) {
    config = Object.assign({},  config, buttonStateConfig[activeNode.nodeName]);
  }

  // Fallback
  if (!config) {
    config = buttonStateConfig.disabled;
  }

  Array.prototype.filter.call(toolbar.getElementsByTagName('button'), (button) => {
    if (config[button.className] === BUTTON_ENABLED) {
      button.removeAttribute('disabled');
      button.removeAttribute('data-active');
    }
    else if (config[button.className] === BUTTON_ACTIVE) {
      button.removeAttribute('disabled');
      button.setAttribute('data-active', 'active')
    } else {
      button.setAttribute('disabled', 'disabled');
      button.removeAttribute('data-active');
    }
  });
};

export default initializeToolbar;