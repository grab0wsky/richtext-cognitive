import { subscribeToParagraphChange, getCurrentParagraph, unselectParagraph } from './paragraphs';

let tooltip = null, slateAdd = null, previousParagraph = null;

let initializeTooltip = () => {
  // Create tooltip
  tooltip = document.createElement('BUTTON');
  let icon = document.createElement('I');
  icon.className = 'material-icons';
  icon.appendChild(document.createTextNode('playlist_add'));
  tooltip.className = 'richtext-editor-tooltip';
  tooltip.appendChild(icon);

  // Add listeners
  tooltip.addEventListener('mouseup', tooltipListener, false);

  // Append tooltip to body
  Array.prototype.filter.call(document.getElementsByTagName('body'), (body) => body.appendChild(tooltip));

  // Subscribe to paragraph changes
  subscribeToParagraphChange(clearSlateAdd);
  subscribeToParagraphChange(moveTooltip);
};

let tooltipListener = (event) => {
  event.preventDefault();
  event.stopPropagation();

  // Add slate
  let currentParagraph = getCurrentParagraph();
  let parentNode = currentParagraph.parentNode;
  slateAdd = document.createElement('DIV');
  slateAdd.className = 'slate slate-add';
  slateAdd.setAttribute('contenteditable', 'false');
  slateAdd.innerHTML =
    '<button role="button"><i class="material-icons">image</i><span>Image</span></button>' +
    '<button role="button"><i class="material-icons">videocam</i><span>Video</span></button>' +
    '<button role="button"><i class="material-icons">art_track</i><span>Teaser</span></button>' +
    '<button role="button"><i class="material-icons">link</i><span>Embed</span></button>';

  parentNode.insertBefore(slateAdd, currentParagraph);

  // Attach handlers
  Array.prototype.filter.call(slateAdd.getElementsByTagName('button'), button => button.addEventListener('mousedown', handleSlateClick));

  // Unselect paragraph & hide tooltip
  previousParagraph = currentParagraph;
  unselectParagraph();
  hideTooltip();
};

let moveTooltip = (node) => {
  if (node === null) {
    hideTooltip();
    return;
  }

  let bodyRect = document.body.getBoundingClientRect(), rect = node.getBoundingClientRect();
  let topOffset = rect.top - bodyRect.top;
  tooltip.style.top = (topOffset+20) + 'px';
  tooltip.style.left = (rect.left-40) + 'px' ;
  tooltip.style.display = 'block';
};

let clearSlateAdd = () => {
  // If slate to add content is shown, remove it first
  if (slateAdd) {
    slateAdd.parentNode.removeChild(slateAdd);
    slateAdd = null;
  }
};

let hideTooltip = () => {
  tooltip.style.display = 'none';
};

let handleSlateClick = event => {
  event.stopPropagation();
  event.preventDefault();
  let resolveButtonType = (element) => element.nodeName === 'BUTTON' ? element.getElementsByTagName('i')[0].innerHTML : resolveButtonType(element.parentNode);
  let type = resolveButtonType(event.target);

  if (previousParagraph) {
    let newNode = document.createElement('DIV');
    newNode.setAttribute('contenteditable', false);
    newNode.className = 'slate slate-' + type;
    newNode.innerHTML = '<i class="material-icons close">close</i><div class="slate-frame"><i class="material-icons">' + type + '</i></div>';
    newNode.getElementsByTagName('i')[0].addEventListener('mousedown', (event) => {
      event.stopPropagation();
      previousParagraph.parentNode.removeChild(newNode);
    });
    previousParagraph.parentNode.insertBefore(newNode, previousParagraph);
  }

};

export default initializeTooltip;